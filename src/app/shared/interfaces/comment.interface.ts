import { Post } from "./post.interface";
import { User } from "./user.interface";

export interface Comment {
    content: string
    createdBy: User
    createdAt: string
    repliesTo: Post
}