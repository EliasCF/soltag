export interface Pagination<T> {
    docs: T[]
    total: number
    limit: number
    offset: number
}