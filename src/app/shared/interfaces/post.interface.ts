import { Tag } from "./tag.interface";
import { User } from "./user.interface";

export interface Post {
    _id: string
    title: string
    content: string
    createdAt: string
    updatedAt: string
    postedBy: User
    tags: Tag[]
}