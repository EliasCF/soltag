import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pagination } from '@app/shared/interfaces/pagination.interface';
import { Post } from '@app/shared/interfaces/post.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  posts: Pagination<Post> = {} as Pagination<Post>;

  constructor(
    private http: HttpClient,
    private router: Router) { }

  ngOnInit() {
    this.http.get<Pagination<Post>>('/api/posts').subscribe(response => this.posts = response);
  }

  redirectToPost(id: string) {
    this.router.navigateByUrl(`/post/${id}`);
  }
}
