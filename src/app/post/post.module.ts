import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { ShowdownModule } from 'ngx-showdown';

const routes: Routes = [
  {
    path: '',
    component: PostComponent 
  }
];

@NgModule({
  declarations: [
    PostComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule, 
    ShowdownModule.forRoot({ smartIndentationFix: true, flavor: 'github', noHeaderId: true })
  ]
})
export class PostModule { }
