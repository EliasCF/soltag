const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const TagSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    }
});

TagSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Tag', TagSchema);