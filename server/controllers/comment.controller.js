const Joi = require('joi');
const Comment = require('../models/comment.model');

const commentSchema = Joi.object({
    content: Joi.string().required(),
    createdAt: Joi.date(),
    postedBy: Joi.string().required(),
    repliesTo: Joi.string().required()
});

async function get(page, count) {
    return await Comment
        .paginate({}, { 
            offset: page, 
            limit: count,
            populate: 'postedBy',
            sort: { createdAt: 'descending' }
        });
}

async function getById(id) {
    return await Comment
        .findById(id)
        .populate('postedBy'); 
}

async function insert(comment) {
    comment = await Joi.validate(comment, commentSchema, { abortEarly: false });
    return await new Comment(comment).save();
}

async function update(comment) {
    comment = await Joi.validate(comment, commentSchema, { abortEarly: false });
    return await Comment.updateOne({ _id: comment.id }, comment);
}

async function deleteById(id) {
    return await Comment.deleteOne({ _id: id });
}

module.exports = {
    get,
    getById,
    insert,
    update,
    deleteById
}