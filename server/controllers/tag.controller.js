const Joi = require('joi');
const Tag = require('../models/tag.model');

const tagSchema = Joi.object({
    title: Joi.string().required(),
});

async function get(page, limit) {
    return await Tag
        .paginate({}, { 
            offset: page, 
            limit: limit,
            sort: { title: 'ascending' }
        });
}

async function getById(id) {
    return await Tag.findById(id); 
}

async function insert(tag) {
    tag = await Joi.validate(tag, tagSchema, { abortEarly: false });
    return await new Tag(tag).save();
}

async function update(tag) {
    tag = await Joi.validate(tag, tagSchema, { abortEarly: false });
    return await Tag.updateOne({ _id: tag.id }, tag);
}

async function deleteById(id) {
    return await Tag.deleteOne({ _id: id });
}

module.exports = {
    get,
    getById,
    insert,
    update,
    deleteById
}
