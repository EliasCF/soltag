const Joi = require('joi');
const Post = require('../models/post.model');
const Tag = require('../models/tag.model');

const postSchema = Joi.object({
    title: Joi.string().required(),
    content: Joi.string(),
    createdAt: Joi.date(),
    updatedAt: Joi.date(),
    postedBy: Joi.string().required()
});

async function get(page, limit) {
    return await Post
        .paginate({}, { 
            offset: page, 
            limit: limit,
            populate: ['postedBy', 'tags'],
            sort: { createdAt: 'descending' }
        });
}

async function getById(id) {
    return await Post
        .findById(id)
        .populate('postedBy')
        .populate('tags');
}

async function insert(post) {
    post = await Joi.validate(post, postSchema, { abortEarly: false });
    return await new Post(post).save();
}

async function update(post) {
    post = await Joi.validate(post, postSchema, { abortEarly: false });
    return await Post.updateOne({ _id: post.id }, post);
}

async function deleteById(id) {
    return await Post.deleteOne({ _id: id });
}

async function addTagToPost(postId, tagId) {
    let post = await Post.findById(postId);
    let tag = await Tag.findById(tagId);

    post.tags.push(tag);
    return await post.save()
}


module.exports = {
    get,
    getById,
    insert,
    update,
    deleteById,
    addTagToPost
}