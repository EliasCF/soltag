const express = require('express');
const asyncHandler = require('express-async-handler');
const passport = require('passport');
const tagController = require('../controllers/tag.controller');

const router = express.Router();
module.exports = router;

router.use(passport.authenticate('jwt', { session: false }));

router.route('/')
    .get(asyncHandler(get))
    .post(asyncHandler(insert))
    .put(asyncHandler(update));

router.route('/:id')
    .get(asyncHandler(getById))
    .delete(asyncHandler(deleteById));

async function get(req, res) {
    let page = parseInt(req.query.page) ? parseInt(req.query.page) : 0;
    let limit = parseInt(req.query.limit) ? parseInt(req.query.limit) : 10;

    try {
        let tags = await tagController.get(page, limit);
        res.json(tags);
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function getById(req, res) {
    try {
        let tag = await tagController.getById(req.params.id);
        res.status(201).json(tag);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}

async function insert(req, res) {
    let tag = await tagController.insert(req.body);
    res.json(tag);
}

async function update(req, res) {
    try {
        let tag = await tagController.update(req.body);
        res.json(tag);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}

async function deleteById(req, res) {
    try {
        await tagController.deleteById(req.params.id);
        res.status(204).send();
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}
