const express = require('express');
const asyncHandler = require('express-async-handler');
const passport = require('passport');
const postController = require('../controllers/post.controller');

const router = express.Router();
module.exports = router;

router.use(passport.authenticate('jwt', { session: false }));

router.route('/')
    .get(asyncHandler(get))
    .post(asyncHandler(insert))
    .put(asyncHandler(update));

router.route('/:id')
    .get(asyncHandler(getById))
    .delete(asyncHandler(deleteById));

router.route('/:postId/tags/:tagId')
    .put(asyncHandler(addTagToPost));

async function get(req, res) {
    let page = parseInt(req.query.page) ? parseInt(req.query.page) : 0;
    let limit = parseInt(req.query.limit) ? parseInt(req.query.limit) : 10;

    try {
        let posts = await postController.get(page, limit);
        res.json(posts);
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function getById(req, res) {
    try {
        let post = await postController.getById(req.params.id);
        res.status(201).json(post);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}

async function insert(req, res) {
    let post = await postController.insert(req.body);
    res.json(post);
}

async function update(req, res) {
    try {
        let post = await postController.update(req.body);
        res.json(post);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}

async function deleteById(req, res) {
    try {
        await postController.deleteById(req.params.id);
        res.status(204).send();
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function addTagToPost(req, res) {
    try {
        let post = await postController.addTagToPost(req.params.postId, req.params.tagId);
        res.json(post);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}
