const express = require('express');
const asyncHandler = require('express-async-handler');
const passport = require('passport');
const commentController = require('../controllers/comment.controller');

const router = express.Router();
module.exports = router;

router.use(passport.authenticate('jwt', { session: false }));

router.route('/')
    .get(asyncHandler(get))
    .post(asyncHandler(insert))
    .put(asyncHandler(update));

router.route('/:id')
    .get(asyncHandler(getById))
    .delete(asyncHandler(deleteById));

async function get(req, res) {
    let page = parseInt(req.query.page) ? parseInt(req.query.page) : 0;
    let limit = parseInt(req.query.limit) ? parseInt(req.query.limit) : 10;

    try {
        let comments = await commentController.get(page, limit);
        res.json(comments);
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function getById(req, res) {
    try {
        let comment = await commentController.getById(req.params.id);
        res.status(201).json(comment);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}

async function insert(req, res) {
    let comment = await commentController.insert(req.body);
    res.json(comment);
}

async function update(req, res) {
    try {
        let comment = await commentController.update(req.body);
        res.json(comment);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
}

async function deleteById(req, res) {
    try {
        await commentController.deleteById(req.params.id);
        res.status(204).send();
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}
