var faker = require('faker');
var Tag = require('./models/tag.model');
var Post = require('./models/post.model');
var User = require('./models/user.model');
var Comment = require('./models/comment.model');

async function generateDataSeeding() {
    faker.seed(123);

    let tags = await generateTags(10);

    let posts = await generatePosts(10, tags);

    generateComments(5, posts);
}

async function generateTags(tagCount) {
    let tags = [];

    for (i = 0; i < tagCount; i++) {
        const tag = { title: faker.company.companyName() };

        const createdTag = await Tag.create(tag);
        tags.push(createdTag);
    }

    return tags;
}

async function generatePosts(postCount, tags) {
    let posts = [];

    const users = await User.find();

    for (i = 0; i < postCount; i++) {
        const post = {
            title: faker.company.catchPhrase(),
            content: faker.lorem.paragraph(5),
            postedBy: users[Math.floor(Math.random() * users.length)],
            tags: [
                tags[Math.floor(Math.random() * tags.length)]
            ]
        }

        const createdPost = await Post.create(post);
        posts.push(createdPost);
    }

    return posts;
}

async function generateComments(commentsCountPerPost, posts) {
    let comments = [];

    const users = await User.find();

    for (i = 0; i < posts.length; i++) {
        let post = posts[i];

        for (j = 0; j < commentsCountPerPost; j++) {
            const comment = {
                content: faker.lorem.sentence(5),
                postedBy: users[Math.floor(Math.random() * users.length)],
                repliesTo: post
            }

            const createdComment = await Comment.create(comment);
            comments.push(createdComment);
        }
    }

    return comments;
}

module.exports = generateDataSeeding;